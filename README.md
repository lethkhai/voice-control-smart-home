# Voice-Controlled Smart Home

This project aims to create a smart home system controlled via voice commands. The system includes a backend server, a web interface, and a Flutter mobile application.

## Table of Contents
- [Introduction](#introduction)
- [Features](#features)
- [Architecture](#architecture)
- [Installation](#installation)
- [Usage](#usage)
- [Repositories](#repositories)

## Introduction
Voice-Controlled Smart Home is an IoT project designed to enhance home automation by using voice commands for controlling various home appliances and systems. The project leverages advanced voice recognition technologies and integrates them with smart devices.

## Features
- Voice command recognition and processing
- Control of various smart home devices
- Real-time status monitoring of devices
- User-friendly web and mobile interfaces
- Secure and scalable backend

## Architecture
The project consists of three main components:
1. **Backend Server**: Manages device control, processes voice commands, and handles data storage.
2. **Flutter Mobile Application**: Allows users to control and monitor their smart home devices on the go.

## Installation
### Backend Server
1. Clone the backend server repository:
   ```bash
   git clone https://gitlab.com/lethkhai/smart-home-be.git
2. Navigate to the project directory and install dependencies:
    ```bash
    cd smart-home-be
    npm install
3. Start the server:
    ```bash
    npm run start
### Flutter Mobile Application
1. Clone the web interface repository:
    ```bash
    git clone https://gitlab.com/lethkhai/voice-recognition.git
2. Navigate to the project directory and install dependencies:
    ```bash
    cd voice-recognition
    flutter pub get
3. Start the app:
    ```bash
    flutter run
## Usage
- Ensure the backend server is running.
- Use the Flutter mobile app on your device to control and monitor smart home devices.

## Repositories
- Backend Server: (https://gitlab.com/lethkhai/smart-home-be.git)
- Flutter Mobile Application: (https://gitlab.com/lethkhai/voice-recognition.git)